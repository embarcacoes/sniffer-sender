#ifndef COMMUNICATION_H
#define COMMUNICATION_H

#ifdef __cplusplus
extern "C"
{
#endif
// Incluindo Bibliotecas
#include <stdio.h>
#include <stdint.h>
#include <stddef.h>
#include <string.h>
#include <stdlib.h>

#include "freertos/FreeRTOS.h"     // FreeRTOS
#include "freertos/task.h"         // Task
#include "freertos/event_groups.h" // Event Groups

#include "esp_system.h" // ESP32
#include "esp_wifi.h"   // Biblioteca do Wifi
#include "esp_wpa2.h"
#include "esp_event.h" // Biblioteca de Eventos
#include "esp_log.h"   // Biblioteca de Log
#include "esp_netif.h"

#include "lwip/err.h" // Biblioteca de Erros
#include "lwip/sys.h" // Biblioteca de Sistema

#include "mqtt_client.h" // Biblioteca MQTT

    // Variaveis e objetos globais

#define WIFI_CONNECTED_BIT BIT0 // Bit 0 - Wifi conectado
#define WIFI_FAIL_BIT BIT1      // Bit 1 - Falha na conexão
#define maximum_retry 50        // Quantidade máxima de tentativas de conexão

#define AP_MODE 1      // Modo AP
#define STATION_MODE 2 // Modo Station


void event_handler(void *arg, esp_event_base_t event_base, int32_t event_id, void *event_data);
void wifi_init_sta(void);

//void setcallback(void (*_callback)(char *, char *, unsigned int, unsigned int));

void log_error_if_nonzero(const char *message, int error_code);

void mqtt_event_handler(void *handler_args, esp_event_base_t base, int32_t event_id, void *event_data);

void mqtt_start(void);


int send_message(char *topic, char message[]);


void stop_mqtt(void); // Função para parar o MQTT

void restart_mqtt(void); // Função para reiniciar o MQTT

#ifdef __cplusplus
}
#endif

#endif