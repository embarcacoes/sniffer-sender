#include <errno.h>
#include <stdio.h>
#include <stdint.h>
#include <stddef.h>
#include <string.h>

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_wifi.h"
#include "esp_err.h"

#include "esp_system.h"
#include "esp_log.h"
#include "nvs_flash.h"

#include "driver/uart.h"
#include "driver/gpio.h"

#include "communication.h" // Biblioteca de comunicação

#define UART_NUM UART_NUM_1
#define UART_BAUD_RATE 115200
#define TXD_PIN (GPIO_NUM_4)
#define RXD_PIN (GPIO_NUM_5)

const char *TAG = "Data-Sender";

const int RX_BUF_SIZE = 512;
const int RECEIVED_BUF_SIZE = 25;
const int DATA_QUEUE_SIZE = 10; // Tamanho da fila de dados a serem enviados

QueueHandle_t data_queue; // Fila responsavel pela comunicação das tasks de UART e MQTT

void init_uart(void);
int sendData(const char *logName, const char *data);
static void tx_task(void *arg);
static void rx_task(void *arg);
static void mqtt_task(void *pvParameters);

void app_main(void)
{
    ESP_LOGI(TAG, "[APP] Startup..");
    ESP_LOGI(TAG, "[APP] IDF version: %s", esp_get_idf_version());

    esp_log_level_set("*", ESP_LOG_INFO);
    esp_log_level_set("MQTT_CLIENT", ESP_LOG_VERBOSE);
    esp_log_level_set("TRANSPORT_BASE", ESP_LOG_VERBOSE);
    esp_log_level_set("TRANSPORT", ESP_LOG_VERBOSE);
    esp_log_level_set("OUTBOX", ESP_LOG_VERBOSE);

    esp_err_t ret = nvs_flash_init();                                             // Inicia a memoria NVS (Responsavel por salvar valores de forma criptografada na flash)
    if (ret == ESP_ERR_NVS_NO_FREE_PAGES || ret == ESP_ERR_NVS_NEW_VERSION_FOUND) // Caso a NVS não esteja alocada, ou esteja cheia ele a formata
    {
        ESP_ERROR_CHECK(nvs_flash_erase()); // Formata a NVS
        ret = nvs_flash_init();             // Inicia a NVS
    }
    ESP_ERROR_CHECK(ret);

    ESP_LOGI(TAG, "Initializing...");

    init_uart(); // Configura a comunicação via UART

    ESP_LOGI(TAG, "Iniciando WiFi...");

    wifi_init_sta(); // Configura e inicia o WiFi

    ESP_LOGI(TAG, "WiFi Iniciado...");

    vTaskDelay(10000 / portTICK_PERIOD_MS); // Espero 10 segundos

    xTaskCreate(rx_task, "uart_rx_task", 8192, NULL, configMAX_PRIORITIES - 2, NULL); // Cria a task de recepção de dados via UART
    xTaskCreate(tx_task, "uart_tx_task", 4096, NULL, configMAX_PRIORITIES - 1, NULL); // Cria a task de envio de dados via UART
    xTaskCreate(mqtt_task, "mqtt_task", 4096, NULL, 10, NULL);                        // Cria a task responsavel pelo mqtt

    ESP_LOGI(TAG, "Rotina de inicialização concluida...");
}

void init_uart(void)
{
    /*
    Função responsavel por configurar e iniciar a UART
    */
    const uart_config_t uart_config = {
        // Struct com as confiugrações da UART
        .baud_rate = UART_BAUD_RATE,
        .data_bits = UART_DATA_8_BITS,
        .parity = UART_PARITY_DISABLE,
        .stop_bits = UART_STOP_BITS_1,
        .flow_ctrl = UART_HW_FLOWCTRL_DISABLE,
        .source_clk = UART_SCLK_DEFAULT,
    };
    uart_driver_install(UART_NUM, RX_BUF_SIZE * 2, 0, 0, NULL, 0);                    // Escolhe a UART a ser utilizada
    uart_param_config(UART_NUM, &uart_config);                                        // Configura a UART com base dos parametros do struct
    uart_set_pin(UART_NUM, TXD_PIN, RXD_PIN, UART_PIN_NO_CHANGE, UART_PIN_NO_CHANGE); // Configura os pinos da UART

    data_queue = xQueueCreate(DATA_QUEUE_SIZE, sizeof(char) * RECEIVED_BUF_SIZE); // Cria uma fila de envio de dados
}

int sendData(const char *logName, const char *data)
{
    /*
    Função responsavel por enviar dados via UART
    */
    const int len = strlen(data);                              // Obtenho o tamanho da string recebido
    const int txBytes = uart_write_bytes(UART_NUM, data, len); // Escreve os bytes na UART
    ESP_LOGI(logName, "Wrote %d bytes", txBytes);
    return txBytes; // Retorna a quantidade de bytes enviados
}

static void tx_task(void *arg)
{
    /*
    Task responsavel por realizar o envio dos dados via UART
    */
    static const char *TX_TASK_TAG = "TX_TASK";   // Crio uma tag de log especifica para os dados enviados
    esp_log_level_set(TX_TASK_TAG, ESP_LOG_INFO); // Seto com uma cor de log diferente
    while (1)                                     // Repete infinitamente
    {
        sendData(TX_TASK_TAG, "Hello world");
        vTaskDelay(2000 / portTICK_PERIOD_MS); // Espera 2 segundos
    }
}

static void rx_task(void *arg)
{
    /*
    Task responsavel por ouvir e receber os dados da UART
    */
    static const char *RX_TASK_TAG = "RX_TASK";         // Crio uma tag de log especifica para os dados recebidos
    esp_log_level_set(RX_TASK_TAG, ESP_LOG_INFO);       // Seto com uma cor de log diferente
    uint8_t *data = (uint8_t *)malloc(RX_BUF_SIZE + 1); // Aloco o buffer para receber informações
    while (1)                                           // Repito infinitamente
    {
        const int rxBytes = uart_read_bytes(UART_NUM, data, RX_BUF_SIZE, 1000 / portTICK_PERIOD_MS); // Realiza a leitura do buffer da UART
        if (rxBytes > 0)                                                                             // Verifica se tem algo no buffer
        {
            data[rxBytes] = 0; // Coloca a ultima posição do buffer após os dados como para sinalizar que ali e o fim da string

            xQueueSend(data_queue, (void *)data, (TickType_t)0); // Coloca os dados na fila para serem envidos via MQTT
        }
    }
    free(data); // Desaloca o BUFFER
}
/*
void callback(char *topic, char *payload, unsigned int topic_lenght, unsigned int length)
{                                                                                      // Callback de recebimento de mensagem
    ESP_LOGI(TAG, "TOPIC: %.*s, PAYLOAD: %.*s", topic_lenght, topic, length, payload); // Log de debug com o topico e o payload
}
*/
static void mqtt_task(void *pvParameters)
{
    /*
    Task responsavel por configurar e enviar dados vi MQTT
    */
    ESP_LOGI(TAG, "Iniciando Task MQTT"); // Log de inicialização da task MQTT

    ESP_LOGI(TAG, "Iniciando MQTT"); // Log de inicialização do servidor MQTT

    // setcallback(callback); // Seta o callback de recebimento de mensagens

    mqtt_start(); // Inicialização do servidor MQTT

    ESP_LOGI(TAG, "MQTT Iniciado"); // Log de inicialização do servidor MQTT

    ESP_LOGI(TAG, "Iniciando Loop de envio de dados."); // Log de inicialização

    vTaskDelay(1000 / portTICK_PERIOD_MS); // Espera um segundo

    char payload[RECEIVED_BUF_SIZE]; // Cria o payload pars receber os dados da fila

    memset(payload, 0, sizeof(payload));

    for (;;)
    {                                    // Loop Infinito
        ESP_LOGI(TAG, "Enviando dados"); // Log de envio de dados

        if (xQueueReceive(data_queue, &payload, portMAX_DELAY))
        {                                      // Recebe os dados dos sensores
            send_message("payload/", payload); // Envia a mensagem
        }

        vTaskDelay(150 / portTICK_PERIOD_MS); // delay de 10 segundos
    }

    vTaskDelete(NULL); // Deleta a task
}
