#include "communication.h" // Incluindo o arquivo de cabeçalho

#define CONFIG_ESP_WIFI_AUTH_WPA2_PSK 1

#if CONFIG_ESP_WIFI_AUTH_OPEN
#define ESP_WIFI_SCAN_AUTH_MODE_THRESHOLD WIFI_AUTH_OPEN
#elif CONFIG_ESP_WIFI_AUTH_WEP
#define ESP_WIFI_SCAN_AUTH_MODE_THRESHOLD WIFI_AUTH_WEP
#elif CONFIG_ESP_WIFI_AUTH_WPA_PSK
#define ESP_WIFI_SCAN_AUTH_MODE_THRESHOLD WIFI_AUTH_WPA_PSK
#elif CONFIG_ESP_WIFI_AUTH_WPA2_PSK
#define ESP_WIFI_SCAN_AUTH_MODE_THRESHOLD WIFI_AUTH_WPA2_PSK
#elif CONFIG_ESP_WIFI_AUTH_WPA_WPA2_PSK
#define ESP_WIFI_SCAN_AUTH_MODE_THRESHOLD WIFI_AUTH_WPA_WPA2_PSK
#elif CONFIG_ESP_WIFI_AUTH_WPA3_PSK
#define ESP_WIFI_SCAN_AUTH_MODE_THRESHOLD WIFI_AUTH_WPA3_PSK
#elif CONFIG_ESP_WIFI_AUTH_WPA2_WPA3_PSK
#define ESP_WIFI_SCAN_AUTH_MODE_THRESHOLD WIFI_AUTH_WPA2_WPA3_PSK
#elif CONFIG_ESP_WIFI_AUTH_WAPI_PSK
#define ESP_WIFI_SCAN_AUTH_MODE_THRESHOLD WIFI_AUTH_WAPI_PSK
#endif

static const char *CommunicationTAG = "Comunicacao";

static EventGroupHandle_t s_wifi_event_group;

esp_mqtt_client_handle_t global_client; // Cliente MQTT

int s_retry_num = 0; // Contador de tentativas de conexão

// char ssid[32] = "";                            // SSID da rede
// char password[64] = "";                 // Senha da rede
char ssid[32] = "";                                      // SSID da rede
char password[64] = "";                                  // Senha da rede
char mqtt_server_uri[128] = "mqtt://192.168.0.101:1883"; // Endereço do servidor MQTT
char mqtt_server_ip[18] = "192.168.0.101";               // Endereço IP do servidor MQTT
char mqtt_user[32] = "";                                 // Usuário do servidor MQTT
// char mqtt_server_ip[18] = "192.168.0.1";
char mqtt_password[64] = "";          // Senha do servidor MQTT
int16_t mqtt_server_port = 1883;      // Porta do servidor MQTT
char mqtt_topic[128] = "ola/teste/1"; // Tópico do servidor MQTT
bool use_uri = true;

// communication_config_t communication_config; // Configurações da comunicação
// void (*callback)(char *, char *, unsigned int, unsigned int); // Função de callback

void event_handler(void *arg, esp_event_base_t event_base,
                   int32_t event_id, void *event_data)
{
    if (event_base == WIFI_EVENT && event_id == WIFI_EVENT_STA_START)
    {
        esp_wifi_connect();
    }
    else if (event_base == WIFI_EVENT && event_id == WIFI_EVENT_STA_DISCONNECTED)
    {
        if (s_retry_num < maximum_retry)
        {
            esp_wifi_connect();
            s_retry_num++;
            ESP_LOGI(CommunicationTAG, "retry to connect to the AP");
        }
        else
        {
            xEventGroupSetBits(s_wifi_event_group, WIFI_FAIL_BIT);
        }
        ESP_LOGI(CommunicationTAG, "connect to the AP fail");
    }
    else if (event_base == IP_EVENT && event_id == IP_EVENT_STA_GOT_IP)
    {
        ip_event_got_ip_t *event = (ip_event_got_ip_t *)event_data;
        ESP_LOGI(CommunicationTAG, "got ip:" IPSTR, IP2STR(&event->ip_info.ip));
        s_retry_num = 0;
        xEventGroupSetBits(s_wifi_event_group, WIFI_CONNECTED_BIT);
    }
}

void wifi_init_sta(void)
{
    s_wifi_event_group = xEventGroupCreate();

    ESP_ERROR_CHECK(esp_netif_init());

    ESP_ERROR_CHECK(esp_event_loop_create_default());
    esp_netif_create_default_wifi_sta();

    wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
    ESP_ERROR_CHECK(esp_wifi_init(&cfg));

    esp_event_handler_instance_t instance_any_id;
    esp_event_handler_instance_t instance_got_ip;
    ESP_ERROR_CHECK(esp_event_handler_instance_register(WIFI_EVENT,
                                                        ESP_EVENT_ANY_ID,
                                                        &event_handler,
                                                        NULL,
                                                        &instance_any_id));
    ESP_ERROR_CHECK(esp_event_handler_instance_register(IP_EVENT,
                                                        IP_EVENT_STA_GOT_IP,
                                                        &event_handler,
                                                        NULL,
                                                        &instance_got_ip));

    wifi_config_t wifi_config = {
        .sta = {
            /* Authmode threshold resets to WPA2 as default if password matches WPA2 standards (pasword len => 8).
             * If you want to connect the device to deprecated WEP/WPA networks, Please set the threshold value
             * to WIFI_AUTH_WEP/WIFI_AUTH_WPA_PSK and set the password with length and format matching to
             * WIFI_AUTH_WEP/WIFI_AUTH_WPA_PSK standards.
             */
            .threshold.authmode = ESP_WIFI_SCAN_AUTH_MODE_THRESHOLD,
            .sae_pwe_h2e = WPA3_SAE_PWE_BOTH,
        },
    };

    strcpy((char *)wifi_config.sta.ssid, ssid);         // Define o SSID do Wi-Fi
    strcpy((char *)wifi_config.sta.password, password); // Define a senha do Wi-Fi

    ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_STA));
    ESP_ERROR_CHECK(esp_wifi_set_config(WIFI_IF_STA, &wifi_config));
    ESP_ERROR_CHECK(esp_wifi_start());

    ESP_LOGI(CommunicationTAG, "wifi_init_sta finished.");

    /* Waiting until either the connection is established (WIFI_CONNECTED_BIT) or connection failed for the maximum
     * number of re-tries (WIFI_FAIL_BIT). The bits are set by event_handler() (see above) */
    EventBits_t bits = xEventGroupWaitBits(s_wifi_event_group,
                                           WIFI_CONNECTED_BIT | WIFI_FAIL_BIT,
                                           pdFALSE,
                                           pdFALSE,
                                           portMAX_DELAY);

    /* xEventGroupWaitBits() returns the bits before the call returned, hence we can test which event actually
     * happened. */
    if (bits & WIFI_CONNECTED_BIT)
    {
        ESP_LOGI(CommunicationTAG, "connected to ap SSID:%s password:%s",
                 ssid, password);
    }
    else if (bits & WIFI_FAIL_BIT)
    {
        ESP_LOGI(CommunicationTAG, "Failed to connect to SSID:%s, password:%s",
                 ssid, password);
    }
    else
    {
        ESP_LOGE(CommunicationTAG, "UNEXPECTED EVENT");
    }
}

// MQTT
/*
void setcallback(void (*_callback)(char *, char *, unsigned int, unsigned int))
{                         // Função para definir o callback de recebimento de mensagem){
    callback = _callback; // Define o callback de recebimento de mensagem
}
*/
void log_error_if_nonzero(const char *message, int error_code) // Função que loga erros
{
    if (error_code != 0)
    {                                                                           // Se o erro não for zero
        ESP_LOGE(CommunicationTAG, "Last error %s: 0x%x", message, error_code); // Log de debug com o erro e o código
    }
}

void mqtt_event_handler(void *handler_args, esp_event_base_t base, int32_t event_id, void *event_data)
{
    // ESP_LOGD(CommunicationTAG, "Event dispatched from event loop base=%s, event_id=%d", base, event_id);
    esp_mqtt_event_handle_t event = event_data;
    esp_mqtt_client_handle_t client = event->client;
    int msg_id;
    switch ((esp_mqtt_event_id_t)event_id)
    {
    case MQTT_EVENT_CONNECTED:
        ESP_LOGI(CommunicationTAG, "MQTT_EVENT_CONNECTED");

        msg_id = esp_mqtt_client_publish(client, mqtt_topic, "data_3", 0, 1, 0);
        ESP_LOGI(CommunicationTAG, "sent publish successful, msg_id=%d", msg_id);

        msg_id = esp_mqtt_client_subscribe(client, "receive/", 0);                      // Subscrição ao topico
        ESP_LOGI(CommunicationTAG, "enviado inscrição com sucesso, msg_id=%d", msg_id); // Log de debug com o id da mensagem

        break;
    case MQTT_EVENT_DISCONNECTED:
        ESP_LOGI(CommunicationTAG, "MQTT_EVENT_DISCONNECTED");

        break;

    case MQTT_EVENT_SUBSCRIBED:
        ESP_LOGI(CommunicationTAG, "MQTT_EVENT_SUBSCRIBED, msg_id=%d", event->msg_id); // Log de debug com o id da mensagem
        msg_id = esp_mqtt_client_publish(client, mqtt_topic, "conectado", 0, 0, 0);    // Publica mensagem no topico
        ESP_LOGI(CommunicationTAG, "sent publish successful, msg_id=%d", msg_id);      // Log de debug com o id da mensagem
        break;
    case MQTT_EVENT_UNSUBSCRIBED:
        ESP_LOGI(CommunicationTAG, "MQTT_EVENT_UNSUBSCRIBED, msg_id=%d", event->msg_id);
        break;
    case MQTT_EVENT_PUBLISHED:
        ESP_LOGI(CommunicationTAG, "MQTT_EVENT_PUBLISHED, msg_id=%d", event->msg_id);
        break;
    case MQTT_EVENT_DATA:
        ESP_LOGI(CommunicationTAG, "MQTT_EVENT_DATA");
        printf("TOPIC=%.*s\r\n", event->topic_len, event->topic); // Log de debug com o topico
        printf("DATA=%.*s\r\n", event->data_len, event->data);    // Log de debug com o payload

        // callback(event->topic, event->data, event->topic_len, event->data_len); // Chama o callback de recebimento de mensagem
        break;
    case MQTT_EVENT_ERROR:
        ESP_LOGI(CommunicationTAG, "MQTT_EVENT_ERROR");
        if (event->error_handle->error_type == MQTT_ERROR_TYPE_TCP_TRANSPORT)
        {
            log_error_if_nonzero("reported from esp-tls", event->error_handle->esp_tls_last_esp_err);
            log_error_if_nonzero("reported from tls stack", event->error_handle->esp_tls_stack_err);
            log_error_if_nonzero("captured as transport's socket errno", event->error_handle->esp_transport_sock_errno);
            ESP_LOGI(CommunicationTAG, "Last errno string (%s)", strerror(event->error_handle->esp_transport_sock_errno));
        }
        break;
    default:
        ESP_LOGI(CommunicationTAG, "Other event id:%d", event->event_id);
        break;
    }
}

void mqtt_start(void) // Inicia o MQTT
{

    if (use_uri)
    {                                                                          // Se o uso de URI
        ESP_LOGI(CommunicationTAG, "MQTT: Connecting to %s", mqtt_server_uri); // Log de debug

        const esp_mqtt_client_config_t mqtt_cfg = {
            .broker.address.uri = mqtt_server_uri,                // URI do servidor
            .credentials.username = mqtt_user,                    // Usuário do servidor
            .credentials.authentication.password = mqtt_password, // Senha do servidor
        };                                                        // Configurações do MQTT

        global_client = esp_mqtt_client_init(&mqtt_cfg);                                           // Inicializa o cliente MQTT
        esp_mqtt_client_register_event(global_client, ESP_EVENT_ANY_ID, mqtt_event_handler, NULL); // Registra o callback de eventos
        esp_mqtt_client_start(global_client);                                                      // Inicia o cliente MQTT
    }
    else
    {                                                                                              // Se o uso de endereço IP
        ESP_LOGI(CommunicationTAG, "MQTT: Connecting to %s:%d", mqtt_server_ip, mqtt_server_port); // Log de debug
        const esp_mqtt_client_config_t mqtt_cfg = {
            .broker.address.hostname = mqtt_server_ip,            // Endereço IP do servidor
            .broker.address.port = mqtt_server_port,              // Porta do servidor
            .credentials.username = mqtt_user,                    // Usuário do servidor
            .credentials.authentication.password = mqtt_password, // Senha do servidor
        };                                                        // Configurações do MQTT

        global_client = esp_mqtt_client_init(&mqtt_cfg);                                           // Inicializa o cliente MQTT
        esp_mqtt_client_register_event(global_client, ESP_EVENT_ANY_ID, mqtt_event_handler, NULL); // Registra o callback de eventos
        esp_mqtt_client_start(global_client);                                                      // Inicia o cliente MQTT
    }
}

int send_message(char *topic, char *message)
{                                                                                          // Envia mensagem
    ESP_LOGI(CommunicationTAG, "Enviando mensagem ao topico %s: %s", mqtt_topic, message); // Log de debug

    int msg_id = esp_mqtt_client_publish(global_client, mqtt_topic, message, 0, 0, 0); // Envia a mensagem

    ESP_LOGI(CommunicationTAG, "Mensagem enviada com sucesso, msg_id=%d", msg_id); // Log de debug

    return msg_id; // Retorna o id da mensagem
}

void stop_mqtt(void)
{                                        // Para o MQTT
    esp_mqtt_client_stop(global_client); // Para o cliente MQTT
}

void restart_mqtt(void)
{                                         // Reinicia o MQTT
    esp_mqtt_client_stop(global_client);  // Para o cliente MQTT
    esp_mqtt_client_start(global_client); // Inicia o cliente MQTT
}